/**
 * Why : i am the middleware for axios and your api request
 * I can help you to write less and aviod TRY.
 * Author : a8
 */
import axios from "axios";

class AxiosPlus {
    constructor({ baseURL = "" }) {
        this.config = {};
        //if url found append url to axios config
        baseURL && (this.config = this.baseURL = { baseURL })
        this.axiosplus = axios.create(this.config);
        this.axiosplus.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

    }

    /** 
     * get broker
     * params : 
     * { 
     *   path : valid url path,
     *   config
     *  }
     */
    get = async ({ path, config = {} }) => {
        try {
            if (path && typeof path == "string") {
                const { data } = await axios.get(path, config);
                return data;
            } else {
                throw new Error("AxiosPlus GET Error: path should be valid string")
            }
        } catch (e) {
            throw e;
        }
    }

    /**
     * post broker
     */
    post = async ({ path, values = {}, config = {} }) => {
        console.log("From post");
        console.log("path")
        console.log(path);
        try {
            if (path && typeof path == "string") {
                const { data } = await axios.post(path, values, config);
                return data;
            } else {
                throw new Error("AxiosPlus POST Error: path should be valid string")
            }
        } catch (e) {
            throw new Error(e);
        }
    }

    /**
     * put broker
     */
    put = async ({ path, params = {}, config = {} }) => {
        try {
            if (path && typeof path == "string") {
                const { data } = await this.axiosplus.put(path, params, config);
                return data;
            } else {
                throw new Error("AxiosPlus PUT Error: path should be valid string")
            }
        } catch (e) {
            throw new Error(e);
        }
    }

    /**
     * delete broker
     */
    delete = async ({ path, params = {}, config = {} }) => {
        try {
            if (path && typeof path == "string") {
                const { data } = await this.axiosplus.delete(path, params, config);
                return data;
            } else {
                throw new Error("AxiosPlus DELETE Error: path should be valid string")
            }
        } catch (e) {
            throw new Error(e);
        }
    }

};

export default new AxiosPlus({ baseURL: "https://esaf.autonom8.com/engine-rest/" });


